#!/bin/bash

e_i() {
	yad --form \
	    --title "Agendamento de backup" \
	    --text "Selecione as opções de agendamento:" \
	    --field="Data e hora: DT" \
	    --field="Comando backup:TXT" \
}

dd=$(e_i)

d_h=$(echo "$dd" | cut -d '|' f -1)
c_b=$(echo "$dd" | cut -d '|' f -2)

echo "$c_b" | at "$d_h"

yad --info --text "backup agendado para $d_h."

