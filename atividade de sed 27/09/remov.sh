#!/bin/bash

if [ -z "$1" ]; then
  echo "Uso: $0 <arquivo>"
  exit 1
fi

sed  -e '/^\s*#/d;/^\s*$/d' -e 's/#.*//' "$1"



