#!/bin/bash

SOURCE_DIR="/home/$(whoami)"
BACKUP_DIR="//tmp/backup"

mkdir -p "$BACKUP_DIR"

TIME=$(date + "%H%M")

BACKUP_FILE="backup_$TIME.tar.gz"

BACKUP_PATH="$BACKUP_DIR/$BACKUP_FILE"

tar czf "$BACKUP_PATH" -C "$SOURCE_DIR" .

if [ $? -eq 0 ]; then
	echo "Backup criado com sucesso: $BACKUP_PATH"
else
	echo "Erro ao criar o backup."

