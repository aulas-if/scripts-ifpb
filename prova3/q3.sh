#!/bin/bash

if [ "$1" == "-c" ]; then
    c=0
    while IFS= read -r l; do
        if [ -z "$l" ]; then
            c=$((c + 1))
        fi
    done < "$2"
    echo "Número de linhas em branco: $c"
else
    
    while IFS= read -r l; do
        if [ -n "$l" ]; then
            echo "$l"
        fi
    done < "$1"
fi


