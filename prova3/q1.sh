#!/bin/bash

if [ -z "$1" ]; then
    echo "Uso: $0 <nu>"
    exit 1
fi

u=$1

while true; do
    echo "Menu de Opções:"
    echo "1 -> Verifica se o usuário existe"
    echo "2 -> Verifica se o usuário está logado na máquina"
    echo "3 -> Lista os arquivos da pasta home do usuário"
    echo "4 -> Sair"
    read -p "Escolha uma opção: " op

    case $op in
        1)
            if id "$u" &>/dev/null; then
                echo "O usuário $u existe."
            else
                echo "O usuário $u não existe."
            fi
            ;;
        2)
            if who | grep -q "^$u "; then
                echo "O usuário $u está logado."
            else
                echo "O usuário $u não está logado."
            fi
            ;;
        3)
            if [ -d "/home/$u" ]; then
                ls "/home/$u"
            else
                echo "A pasta home do usuário $u não existe."
            fi
            ;;
        4)
            echo "Saindo"
            break
            ;;
        *)
            echo "Opção inválida. Tente novamente."
            ;;
    esac
done



