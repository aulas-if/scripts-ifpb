#!/bin/bash

ld() {
    for item in *; do
        if [ -d "$item" ]; then
            echo "$item"
        fi
    done
}

le() {
    for item in *; do
        if [ -x "$item" ] && [ ! -d "$item" ]; then
            echo "$item"
        fi
    done
}

lss() {
    for item in *; do
        if [ -f "$item" ] && [ -x "$item" ] && head -n 1 "$item" | grep -q "^#!/bin/bash"; then
            echo "$item"
        fi
    done 
}
case "$1" in
    -d)
        ld
        ;;
    -e)
        le
        ;;
    -s)
        lss
        ;;
    *)
        echo "Opção inválida."
        ;;
esac



