#!/bin/bash

echo "Digite a senha:"
read pw

um=0
lm=0
nm=0

for (( i=0; i<${#pw}; i++ )); do
    ch="${pw:$i:1}"
    if [[ "$ch" =~ [A-Z] ]]; then
        um=1
    elif [[ "$ch" =~ [a-z] ]]; then
        lm=1
    elif [[ "$ch" =~ [0-9] ]]; then
        nm=1
    fi
done

if [[ $um -eq 1 && $lm -eq 1 && $nm -eq 1 ]]; then
    echo "Senha válida"
else
    echo "Senha inválida"
fi

