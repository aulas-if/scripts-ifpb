#!/bin/bash

if [ -z "$1" ]; then
  echo "forneça o caminho do arquivo."
  exit 1
fi

while IFS= read -r line; do
  
  if echo "$line" | grep -qE 'R\$ [0-9]+(\.[0-9]{3})*,[0-9]{2}'; then
    
    echo "$line" | grep -oE 'R\$ [0-9]+(\.[0-9]{3})*,[0-9]{2}'
  fi
done < "$1"

