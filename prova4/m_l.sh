#!/bin/bash

monitor_log() {
  log_file=$(echo "$1" | tr -d '[:space:]') # Remove espaços em branco
  if [ -f "$log_file" ]; then
    echo "Monitorando: $log_file"
    tail -f "$log_file" | yad --text-info --title="Monitorando $log_file" --width=800 --height=600 &
  else
    yad --error --text="Arquivo não encontrado: $log_file"
  fi
}

if [ -z "$1" ]; then
  yad --error --text="Uso: $0 <arquivo_de_configuracao>"
  exit 1
fi

config_file="$1"

if [ ! -f "$config_file" ]; then
  yad --error --text="Arquivo de configuração não encontrado!"
  exit 1
fi

log_files=$(yad --list --checklist --column="Seleção" --column="Arquivos de Log" $(awk '{print "FALSE", $1}' "$config_file") --width=500 --height=300 --title="Selecionar Arquivos de Log")

if [ -z "$log_files" ]; then
  yad --info --text="Nenhum arquivo selecionado para monitoramento."
  exit 0
fi

for log_file in $(echo "$log_files" | awk -F'|' '{print $2}'); do
  monitor_log "$log_file"
done

wait

